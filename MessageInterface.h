//
// Created by joppe on 25-10-2018.
//

#ifndef CPP_EXERCISE_5_MESSAGES_MESSAGEINTERFACE_H
#define CPP_EXERCISE_5_MESSAGES_MESSAGEINTERFACE_H

#include "Message.h"

template<class T> class MessageInterface
{
public:
    std::stringstream createStringMessage();
    Message(std::string topic, T value);

 };

#endif //CPP_EXERCISE_5_MESSAGES_MESSAGEINTERFACE_H
