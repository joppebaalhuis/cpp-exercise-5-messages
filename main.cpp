
#include <iostream>
#include "Message.h"
#include <vector>
#include "MessageInterface.h"

int main()
{
    int error = 0;
    //  Message<bool> message1;
    Message<bool> m1("/status/health/sens1", true);
    Message<int> m2("/status/health/sens1", 5);
    Message<float> m3("/status/health/sens1", 1.555);
    Message<std::string> m4("/status/health/sens1", "ok");

    std::vector<Message<void *>> msg;

    msg.push_back(m1);


    std::cout << m1 << std::endl;
    std::cout << m2 << std::endl;
    std::cout << m3 << std::endl;
    std::cout << m4 << std::endl;

    return 0;
}