//
// Created by joppe on 22-10-2018.
//

#ifndef ASSIGNMENT_05_MESSAGE_H
#define ASSIGNMENT_05_MESSAGE_H

#include <iostream>
#include <time.h>
#include <ctime>
#include <typeinfo>
#include <sstream>
#include <cxxabi.h>
#include <cstring>
#include "MessageInterface.h"

template <class T> class Message
{
public:
    Message(){}
    Message(std::string topic, T value)
    {
        const std::time_t now = std::time(nullptr);
        this->_time  = *std::localtime(std::addressof(now));
        this->_topic = topic;
        this->_value = value;
    }
    friend std::ostream& operator<<(std::ostream &out, const Message<T>& message)
    {
        Message<T> messageObj(message);
        std::stringstream output = messageObj.createStringMessage();
        out << output.str();
        return out ;
    }

    std::stringstream createStringMessage()
    {
        int error;
        std::stringstream output;;
        char buffer[20];
        std::strftime(buffer, 20, "%H:%M:%S",  &_time);
        std::string timeString(buffer);
        output << timeString <<" | ";                                                               //time
        output << _topic ;                                                                          //topic
        output <<  " | " <<"{ type : ";
        char * typeName = abi::__cxa_demangle(typeid(T).name(), 0, 0, &error);
        //std::cout << typeName << std::endl;
        if(!std::strcmp(typeName, "bool"))                                                                                          //if data type is a bool print boolean
        {
          output<< "\"boolean\"" ;     //type
        }else
        {
            if(!std::strcmp(typeName, "float") || !std::strcmp(typeName, "int") || !std::strcmp(typeName, "double"))                //if data type is an float, int or double print numeric
            {
                output<< "\"numeric\"" ;     //type
            }else
            {
                if(std::strcmp(typeName,"std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char>\n"" >"))     //if the type is an std::string print  string
                {
                    output << "\"string\"";
                }else
                    output << typeName;                                                                                             //if type is none of the above print the 'raw' type name
            }
        }

        output << ", data : \""  <<std::boolalpha <<_value << "\"}";                                                //value
        return output;
    }
//13:19:27.770000 | /status/diag/motor1/desc | { type : "boolean", data : "true"}
    Message(const Message<T> &other)
    {
        this->_time = other._time;
        this->_topic = other._topic;
        this->_value = other._value;
    }

     std::string test()
     {
            std::cout << "working" << std::endl;
     }

private:
    struct tm _time;
    std::string _topic;
    T _value;


};


#endif //ASSIGNMENT_05_MESSAGE_H
